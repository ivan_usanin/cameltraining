package com.nixsolutions.camel;

public interface CamelService {
	/**
	 * Send message to ActvieMQ broker.
	 *
	 * @param message to send.
	 */
	public void send(String message);

	/**
	 * Receive message from ActiveMQ broker.
	 *
	 * @return String received message.
	 */
	public String receive();
}
