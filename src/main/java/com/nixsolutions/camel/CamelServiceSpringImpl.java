package com.nixsolutions.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CamelServiceSpringImpl implements CamelService {
	private static final String IN_ENDPOINT = "direct:in";
    private static final String OUT_ENDPOINT = "activemq:queue:test-queue";

    private CamelContext camelContext;
    private ProducerTemplate producer;
    private ConsumerTemplate consumer;
    
    @SuppressWarnings("resource")
	public CamelServiceSpringImpl() {
        camelContext = (CamelContext) new ClassPathXmlApplicationContext("spring-camel-context.xml").getBean("camelContext");
        producer = camelContext.createProducerTemplate();
        consumer = camelContext.createConsumerTemplate();
    }

    public void send(final String message){
        producer.sendBody(IN_ENDPOINT, message);
    }

    public String receive(){
        return (String) consumer.receiveBody(OUT_ENDPOINT);
    }

}
