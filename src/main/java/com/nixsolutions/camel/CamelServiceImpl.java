package com.nixsolutions.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import static org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent;

public class CamelServiceImpl implements CamelService {
	
	private static final String IN_ENDPOINT = "direct:in";
    private static final String OUT_ENDPOINT = "activemq:queue:test-queue";

    private CamelContext context;
    private ProducerTemplate producer;
    private ConsumerTemplate consumer;       

	public CamelServiceImpl() throws Exception {
		context = new DefaultCamelContext();
		
		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				from(IN_ENDPOINT).to(OUT_ENDPOINT);
			}
		});
		
		context.addComponent("activemq", activeMQComponent("tcp://localhost:61616"));
		
		producer = context.createProducerTemplate();
        consumer = context.createConsumerTemplate();
        
        context.start();
	}

	public void send(final String message) {
		producer.sendBody(IN_ENDPOINT, message);
	}

	public String receive() {
		return (String) consumer.receiveBody(OUT_ENDPOINT);
	}

}
