package com.nixsolutions.camel;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class CamelServiceTest {
	private static final String TEST_STRING = "TEST_STRING";
	
	@Test
    public void testServiceSpring() throws Exception {
		CamelService service = new CamelServiceSpringImpl();

        service.send(TEST_STRING);

        //wait some time
        Thread.sleep(TimeUnit.SECONDS.toMillis(2));

        assertEquals(TEST_STRING, service.receive());
    }
	
	@Test
    public void testServiceJava() throws Exception {
        CamelService service = new CamelServiceImpl();

        service.send(TEST_STRING);

        //wait some time
        Thread.sleep(TimeUnit.SECONDS.toMillis(2));

        assertEquals(TEST_STRING, service.receive());
    }
	
	@Test
	public void testServiceFile() throws Exception{
		CamelService service = new CamelServiceFileImpl();
		
		for (int i = 0; i < 10; i++) {
			service.send("Test Message: " + (i + 1));
		}
	}
}
